¿Que es GIT?

Es un software de control de versiones diseñado por Linus Torvalds teniendo en cuenta la eficiencia, 
la confiabilidad y la compatibilidad en el control de versiones de aplicaciones cuando las aplicaciones 
contienen una gran cantidad de archivos de código fuente. Su propósito es realizar un seguimiento de los 
cambios en los archivos de la computadora, incluida la coordinación del trabajo que muchas personas realizan 
en los archivos compartidos en un único repositorio de código.

Originalmente, Git se diseñó como una herramienta de bajo nivel para que otros pudieran escribir interfaces 
de usuario o interfaces de usuario como Cogito o StGIT. Sin embargo, Git ha evolucionado desde entonces
hasta convertirse en un sistema de control de versiones completamente funcional. Existe una serie de proyectos 
relacionados que ya usan Git, especialmente el paquete de programación del kernel de Linux. Actualmente, 
el mantenimiento del software Git está a cargo de Junio ​​Hamano, quien recibe contribuciones 
de programación de alrededor de 280 programadores. En cuanto a los derechos de autor, Git es software 
libre que se puede distribuir bajo los términos de la versión 2 de la Licencia Pública General GNU.

Importancia de GIT:

Es una gran herramienta para el trabajo en equipo. Por ejemplo, cuando se trata de un software 
o un sitio web integrado, la plataforma en línea definitivamente facilita la gestión de proyectos.
Y la herramienta también está disponible para las empresas: se llama GitHub Enterprise. Encontrará 
una forma inteligente de gestionar el trabajo en equipo.

Además, la seguridad se vuelve algo muy peligroso, lo cual es muy importante cuando se trata de proyectos
digitales. Pero lo que realmente destaca es que el equipo puede trabajar al mismo tiempo, desde diferentes 
lugares, no solo en la misma ciudad, sino en todo el mundo.

Para cualquier negocio actual, la automatización del flujo de trabajo es esencial y GitHub lo ha hecho 
posible. Los recursos de la plataforma ayudan en el desarrollo de proyectos, facilitando el 
crecimiento de toda la empresa.

Comandos más usados:

Git Clone:

Es un comando para descargar el código fuente actual desde un repositorio remoto. 
En otras palabras, Git clon básicamente crea un espejo de la última versión del proyecto en 
un repositorio y lo guarda en su computadora.

git clone <https://link-con-nombre-del-repositorio>

Git checkout: 

También es uno de los comandos más utilizados en Git. Para trabajar en una sucursal, 
primero debe cambiarse a ella. Usaremos principalmente git checkout para llevarlo de 
una sucursal a otra. También podemos usarlo para verificar archivos y confirmaciones.

git checkout <nombre-de-la-rama>

Git status:

El comando de git status nos da toda la información necesaria sobre la rama actual.

Podemos encontrar información como:

Si la rama actual está actualizada
Si hay algo para confirmar, enviar o recibir (pull).
Si hay archivos en preparación (staged), sin preparación(unstaged) 
Si hay archivos creados, modificados o eliminados

<git status>

Git branch: 

Las ramas son muy importantes en el mundo Git. Con las sucursales, varios 
desarrolladores pueden trabajar codo con codo en el mismo proyecto al mismo tiempo. 
Podemos usar el comando git branch para crearlos, insertarlos y eliminarlos.

git branch <nombre-de-la-rama>

Git commit:

Este es probablemente el comando Git más utilizado. Cuando llegamos a cierto punto de 
desarrollo, queremos guardar nuestros cambios (quizás después de una tarea
o problema específico). Git commit es como configurar un punto de control 
durante el desarrollo al que puede volver más tarde si es necesario.

También necesitamos escribir un mensaje corto para explicar lo que hemos desarrollado 
o cambiado en el código fuente.

git commit -m "mensaje de confirmación"

Git add:

También es uno de los comandos más utilizados en Git. Para trabajar en una sucursal, 
primero debe cambiarse a ella. Usaremos principalmente git checkout para llevarlo de 
una sucursal a otra. También podemos usarlo para verificar archivos y confirmaciones.

git add <archivo>

Git push:

Una vez implementados los cambios, el siguiente paso es enviar los cambios 
al servidor remoto. Git push envía tus confirmaciones al repositorio remoto.

git push <nombre-remoto> <nombre-de-tu-rama>

Git pull:

El comando git pull se usa para extraer actualizaciones del repositorio remoto.
 Este comando es una combinación de git fetch y git merge, lo que significa que 
cuando usamos git pull, obtenemos actualizaciones del repositorio remoto (git fetch) 
y aplicamos los cambios de inmediato. Esta última innovación es local (git merge).

git pull <nombre-remoto>
